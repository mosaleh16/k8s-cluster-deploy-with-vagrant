#!/bin/bash

echo "[TASK 1] Join node to Kubernetes Cluster"
apt-get install -qq -y sshpass
sshpass -p "kubeadmin" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no kcontroller.example.com:/joincluster.sh /joincluster.sh >/dev/null 2>&1
bash /joincluster.sh >/dev/null 2>&1
