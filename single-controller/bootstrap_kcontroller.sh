#!/bin/bash

echo "[TASK 1] Pull required containers"
kubeadm config images pull >/dev/null 2>&1

echo "[TASK 2] Initialize Kubernetes Cluster"
kubeadm init --apiserver-advertise-address=192.168.56.20 --ignore-preflight-errors all --pod-network-cidr=192.168.56.0/16 --token-ttl 0 >> /root/kubeinit.log

echo "[TASK 3] Configure Kubectl"
mkdir -p "$HOME"/.kube
sudo cp -i /etc/kubernetes/admin.conf "$HOME"/.kube/config
sudo chown "$(id -u)":"$(id -g)" "$HOME"/.kube/config

echo "[TASK 4] Deploy Weave network"
kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml >/dev/null 2>&1

echo "[TASK 5] Install Helm"
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 >/dev/null 2>&1
chmod 700 get_helm.sh >/dev/null 2>&1
./get_helm.sh >/dev/null 2>&1

echo "[TASK 6] Create a namespace for OpenEBS"
kubectl create ns openebs >/dev/null 2>&1

echo "[TASK 7] Install OpenEBS"
helm repo add openebs https://openebs.github.io/charts >/dev/null 2>&1
helm repo update >/dev/null 2>&1
helm install --namespace openebs openebs openebs/openebs >/dev/null 2>&1

echo "[TASK 8] Generate and save cluster join command to /joincluster.sh"
unset http_proxy
kubeadm token create --print-join-command > /joincluster.sh
