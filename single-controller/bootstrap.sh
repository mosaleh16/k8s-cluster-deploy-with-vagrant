#!/bin/bash

echo "[TASK 1] Disable and turn off SWAP"
sed -i '/swap/d' /etc/fstab
swapoff -a

echo "[TASK 2] Stop and Disable firewall"
systemctl disable --now ufw >/dev/null 2>&1

echo "[TASK 3] Enable and Load Kernel modules"
cat >>/etc/modules-load.d/containerd.conf<<EOF
overlay
br_netfilter
EOF
modprobe overlay
modprobe br_netfilter

echo "[TASK 4] Add Kernel settings"
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system >/dev/null 2>&1

echo "[TASK 5] Install containerd runtime"
apt-get update -qq
apt-get install -qq -y apt-transport-https ca-certificates curl gnupg

CONTAINERD_VERSION=1.7.0
wget https://github.com/containerd/containerd/releases/download/v${CONTAINERD_VERSION}/containerd-${CONTAINERD_VERSION}-linux-amd64.tar.gz >/dev/null 2>&1
sudo tar Czxvf /usr/local containerd-${CONTAINERD_VERSION}-linux-amd64.tar.gz >/dev/null 2>&1
wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service >/dev/null 2>&1
mv containerd.service /usr/lib/systemd/system/
systemctl daemon-reload >/dev/null 2>&1
systemctl enable --now containerd >/dev/null 2>&1

RUNC_VERSION=1.1.4
wget https://github.com/opencontainers/runc/releases/download/v${RUNC_VERSION}/runc.amd64 >/dev/null 2>&1
sudo install -m 755 runc.amd64 /usr/local/sbin/runc >/dev/null 2>&1

CNI_PLUGINS_VERSION=1.2.0
wget https://github.com/containernetworking/plugins/releases/download/v${CNI_PLUGINS_VERSION}/cni-plugins-linux-amd64-v${CNI_PLUGINS_VERSION}.tgz >/dev/null 2>&1
mkdir -p /opt/cni/bin >/dev/null 2>&1
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v${CNI_PLUGINS_VERSION}.tgz >/dev/null 2>&1

echo "[TASK 6] Add apt-get repo for kubernetes"
curl -fsSL  https://packages.cloud.google.com/apt/doc/apt-key.gpg|sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/k8s.gpg >/dev/null 2>&1
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - >/dev/null 2>&1
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list >/dev/null 2>&1


echo "[TASK 7] Install Kubernetes components (kubeadm, kubelet and kubectl)"
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8 >/dev/null 2>&1

apt-get update -qq -y
KUBERNETES_VERSION=1.26.3
apt-get install -qq -y kubeadm=${KUBERNETES_VERSION}-00 kubelet=${KUBERNETES_VERSION}-00 kubectl=${KUBERNETES_VERSION}-00
apt-mark hold kubelet kubeadm kubectl >/dev/null 2>&1


echo "[TASK 8] Install iSCSI"
apt-get install -qq -y open-iscsi
systemctl enable --now iscsid >/dev/null 2>&1
cat /etc/iscsi/initiatorname.iscsi >/dev/null 2>&1
systemctl status iscsid >/dev/null 2>&1

echo "[TASK 9] Enable ssh password authentication"
sed -i 's/^PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
systemctl reload sshd >/dev/null 2>&1

echo "[TASK 10] Set root password"
echo -e "kubeadmin\nkubeadmin" | passwd root
echo -e "kubeadmin\nkubeadmin" | passwd vagrant
echo "export TERM=xterm" >> /etc/bash.bashrc >/dev/null 2>&1

echo "[TASK 11] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.56.20   kcontroller.example.com kcontroller
192.168.56.21   kworker1.example.com    kworker1
192.168.56.22   kworker2.example.com    kworker2
EOF
