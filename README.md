The goal of this repo is to create a Kubernetes Cluster using Kubeadm on VirtualBox VMs with the help of Vagrant.


# Prerequisites

To run our own Kubernetes Cluster locally the following are being used:
- Virtualbox
- Vagrant


# Getting Started

Run the following from the root directory:
```sh
cd vagrant/ && vagrant up
```

If you want to run the `kubectl` commands from you host machine, run the following:

```sh

# password: kubeadmin

scp root@192.168.56.20:/etc/kubernetes/admin.conf ~/.kube/config
```

or else, ssh to the controller VM

```sh
cd single-controller && vagrant ssh kcontroller
```

# Single Controller

In this setup, we are deploying a K8s cluster with 1 control-plane node and 2 worker nodes.

## Container Runtime

Containerd is being using as a Container Runtime.

## CNI

Weave Net is being used as Container Network Interface (CNI) plugin.

## CSI

OpenEBS is being used as Container Storage Interface (CSI) plugin.
